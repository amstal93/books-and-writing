set -e

mongo <<EOF
use books_valentin
use admin
db.auth('$MONGO_INITDB_ROOT_USERNAME', '$MONGO_INITDB_ROOT_PASSWORD')

db = db.getSiblingDB('books_valentin')

db.createUser({
  user: '$MONGO_INITDB_ROOT_USERNAME',
  pwd:  '$MONGO_INITDB_ROOT_PASSWORD',
  roles: [{
    role: 'readWrite',
    db: 'books_valentin'
  }]
})
use books_valentin
EOF