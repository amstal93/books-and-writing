const {model, Schema} = require('mongoose');

const author_schema = new Schema({
    name: String,
})

const Author = model("Author", author_schema);

module.exports = Author;
