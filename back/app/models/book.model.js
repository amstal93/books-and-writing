const {model, Schema} = require('mongoose');

const book_schema = new Schema({
    title: String,
    publishDate: Date,
    addingDate: Date,
    lastModified: Date,
    price: Number,
    rating: Number,
    editor: {
        type: Schema.Types.ObjectId,
        ref: "Book"
    },
    authors: [
        {
            type: Schema.Types.ObjectId,
            ref: "Author",
        }
    ],
    resume: [
        {
            lang: String,
            text: String,
        }
    ],
    review: [
        {
            lang: String,
            text: String,
        }
    ],
    affilated_links: [
        {
            type: Schema.Types.ObjectId,
            ref: "Link"
        }
    ],
    categories: [
        {
            type: Schema.Types.ObjectId,
            ref: "Categorie"
        }
    ]
})

const Book = model("Book", book_schema);

module.exports = Book;
