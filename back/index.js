const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dbConfig = require('./config/db.config');
const db = require('./app/models');

const app = express();

const corsOptions = {
    origin: process.env.NODE_ENV==='production' ? /https:\/\/[\S\.]*boutvalentin\.com/ : '*'  
}
if (process.env.NODE_ENV==='production') {
    app.use(cors(''))
}
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Set routes
require('./app/routes')(app)

db.mongoose.connect(`mongodb://${dbConfig.USER}:${dbConfig.USER_PWD}@${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(()=>{
    console.log("Successfully connect to MongoDB");
}).catch((err)=>{
    console.log("Connection error", err)
    //process.exit()
})

app.get("/", (req, res)=> {
    res.json({message: 'Hello WOrld', query: req.query});
})

const PORT = process.env.NODE_ENV==='production' ? 80 : 8080;
app.listen(PORT, () => {
    console.log(`Serveur is running on ${PORT}`)
})