const { secrets } = require('../../config/auth.config');
const { user: User } = require('../models');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { abstractTokenCheck } = require('../middlewares/authJwt');

const validateInputUser = ({username, email, password}) => {
    if (!username || !email || !password) return false;
    if (!email.match(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)) return false;
    return true;

}

const abstractSender = (res, code, user) =>{
    res.status(code).send({
        id: user._id,
        username: user.username,
        email: user.email,
        accessToken: jwt.sign({id: user._id}, secrets, {expiresIn: 3600}),
    })
}

exports.signUp = (req, res) => {
    console.log(`SignUp handeling`);
    const { username, email, password } = req.body;
    if (!validateInputUser(req.body)) {
        res.status(400).send({message: "Bad Request"});   
        return;
    }
    const user = new User({username, email, password: bcrypt.hashSync(password, 8)})

    user.save((err, user) => {
        if (err) {
            res.status(500).send({message: err});
            return;
        }
        abstractSender(res, 201, user);
    })
}

exports.signIn = (req, res) => {
    const { username, password } = req.body;
    User.findOne({username}).exec((err, user) => {
        if (err) {
            res.status(500).send({message: err});
            return;
        }

        if (!user) {
            return res.status(404).send({message: 'Not found'});
        }

        const validPass = bcrypt.compareSync(password, user.password);

        if(!validPass) {
            return res.status(401).send({
                accessToken: null, 
                message: "Unauthorized"
            })
        }
        abstractSender(res, 200, user);
    })
}

exports.check = (req, res) => {
    const callback = ({req, res}) => {
        res.status(200).send({
            accessToken: req.headers["x-access-token"],
        })
    }
    abstractTokenCheck({req, res, callback});
}