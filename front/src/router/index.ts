import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import SignUp from "../views/SignUp.vue";
import BookSellerAdmin from "../views/BookSellerAdmin.vue";
import BookDetail from '../views/BookDetail.vue';
import ArticleDetail from '../views/ArticleDetail.vue';
import BookList from '../views/BookList.vue';
import ArticleList from '../views/ArticleList.vue';
import Container from '../views/Container.vue';
import { supportLanguages, defaultLanguages} from '../languages/index';

const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    redirect: `/${defaultLanguages}`
  },
  {
    path: '/:locale',
    name: 'locale',
    component: Container,
    beforeEnter: (to, from) => {
      const locale = to.params.locale;  
      console.log(from)
      console.log(`${defaultLanguages}${to.fullPath}`, locale);

      if (!locale || Array.isArray(locale) || !supportLanguages.includes(locale)) return `/${defaultLanguages}${to.fullPath}`;
      // TODO: HANDLE SUPPORT LANGUAGE OR RETURN
      console.log("TA MAA")
      return true;
    },
    // prettier-ignore
    children: [
      {
        path: "",
        name: "Home",
        component: Home,
      },
      {
        path: "legal-notice",
        name: "About",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/About.vue"),
      },
      {
        path: "signup",
        name: "Sign Up",
        component: () => import(/* webpackChunkName: "signup" */ "../views/SignUp.vue"),
      },
      {
        path: "bookseller",
        name: "Bookseller Administration",
        component:  () => import(/* webpackChunkName: "bookselleradmin" */ "../views/BookSellerAdmin.vue"),
        beforeEnter: async (to, from) => {
          const locale = to.params.locale;
          console.log(locale);
          const headers = {
            'x-access-token': sessionStorage.getItem('token') || '',
          }
          try {
            const response = await fetch(`${process.env.VUE_APP_API_URL}api/auth/check`, {method: 'GET', headers});
            if (response.status >= 400 && response.status <=600) {
              sessionStorage.removeItem('token');
              return `/${locale}/signup`;
            }
            const message = await response.json();
            sessionStorage.setItem('token', message.accessToken);
          } catch(err) {
            return `/${locale}/signup`;
          }
        }
      },
      {
        path: "books",
        name: "Book list",
        component: BookList
      },
      {
        path: "books/:slug",
        name: "Book detail",
        component: BookDetail
      },
      {
        path: "articles",
        name: "Articles list",
        component: ArticleList
      },
      {
        path: "articles/:slug",
        name: "Article detail",
        component: ArticleDetail
      },
      {
        // prettier-ignore
        path: ':pathMatch(.*)*',
        name: "Not Found",
        component: () =>
          import(/* webpackChunkName: "notfound" */ "../views/NotFound.vue"),
      }
    ]
  },
  {
    // prettier-ignore
    path: '/:pathMatch(.*)*',
    name: "Not Found",
    component: () =>
      import(/* webpackChunkName: "notfound" */ "../views/NotFound.vue"),
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
